// Utility to send an email notification

package main

import (
    "flag"
	"log"
	"net/smtp"
)

var(
    username    = "user@example.com"
    password    = "password"
    hostname    = "smtp.gmail.com"
    port        = "587"
    dest        = "destination@exemple.com"
    body        = "Checkpoint notification"
    subject     = "Checkpoint notification"
    argFrom     = flag.String("from", username, "Sender information to show in email")
    argDest     = flag.String("to", dest, "Destination of notification")
    argBody     = flag.String("body", body, "Body of the message")
    argSubject  = flag.String("subject", subject, "Subject of the message")
)

func main() {
    flag.Parse()

	// Set up authentication information.
	auth := smtp.PlainAuth("", username, password, hostname)

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	to := []string{*argDest}
	msg := []byte("To: "+*argDest+"\r\n" +
		"Subject: "+*argSubject+"\r\n" +
		"\r\n" +
		*argBody+"\r\n")
	err := smtp.SendMail(hostname+":"+port, auth, *argFrom, to, msg)
	if err != nil {
		log.Fatal(err)
	}
}
